# -*- coding: utf-8 -*-
from functools import wraps, partial

from psycats.control.applicative import Applicative, pure
from psycats.control.bind import Bind
from psycats.data.lazy import lazy


def do(x):
    """Monadic `do` syntax for python
    Inspired by http://www.valuedlessons.com/2008/01/monads-in-python-with-nice-syntax.html
    >>> from psycats.data.maybe import Maybe, Nothing
    >>> @do
    ... def div_maybe(n, d):
    ...     n = yield Maybe(n)
    ...     d = yield pure(d)
    ...     if d == 0:
    ...         yield Nothing
    ...     yield pure(n / d)
    ...
    >>> div_maybe(1, 2)
    Just(0.5)
    >>> div_maybe(1, 0)
    Nothing

    You can explicitly specify the Monadic type, useful if your first yield
    is `pure` -

    >>> @do(Maybe)
    ... def div_maybe(n, d)
    ...     n = yield pure(n)
    ...     d = yield pure(d)
    ...     if d == 0:
    ...         yield Nothing
    ...     yield pure(n / d)


    For Python version which support using `return` in generators, it "just works"
    as expected, mapping the return value into the Monadic context.

    >>> @do
    ... def div_maybe(n, d)
    ...     n = yield Maybe(n)
    ...     d = yield Maybe(d)
    ...     if d == 0:
    ...         yield Nothing
    ...     return n / d
    """
    if isinstance(x, type):
        return partial(_do, x)
    return _do(None, x)


def _do(user_type, f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        it = f(*args, **kwargs)
        m0 = next(it)
        typ = user_type or type(m0)

        get_applicative = lazy(lambda: Applicative[typ])

        def lift_pure(pure_obj):
            return get_applicative().pure(pure_obj.value)

        if isinstance(m0, pure):
            if user_type is None:
                raise TypeError(
                    'Cannot yield pure as first expression without a type hint. '
                    'Use @do(<type>) instead.')
            m0 = lift_pure(m0)

        if hasattr(m0, 'bind'):
            def bind(fa, g): return fa.bind(g)
        else:
            bind = Bind[typ].bind

        def loop(m, x):
            try:
                n = it.send(x)
            except StopIteration as e:
                if e.value is not None:
                    if hasattr(m, 'map'):
                        return m.map(lambda _: e.value)
                    else:
                        return get_applicative().of(e.value, m)
                return m
            if isinstance(n, pure):
                n = lift_pure(n)
            return bind(n, partial(loop, n))

        return bind(m0, partial(loop, m0))
    return wrapper


def _do_iter(it, user_type):
    m0 = next(it)
    typ = user_type or type(m0)

    get_applicative = lazy(lambda: Applicative[typ])

    def lift_pure(pure_obj):
        return get_applicative().pure(pure_obj.value)

    if isinstance(m0, pure):
        if user_type is None:
            raise TypeError(
                'Cannot yield pure as first expression without a type hint. '
                'Use @do(<type>) instead.')
        m0 = lift_pure(m0)

    if hasattr(m0, 'bind'):
        def bind(fa, g): return fa.bind(g)
    else:
        bind = Bind[typ].bind

    def loop(m, x):
        try:
            n = it.send(x)
        except StopIteration:
            return m
        if isinstance(n, pure):
            n = lift_pure(n)
        return bind(n, partial(loop, n))

    return bind(m0, partial(loop, m0))
