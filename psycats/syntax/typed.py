# -*- coding: utf-8 -*-


# noinspection PyPep8Naming
class typed(object):
    def __init__(self, f):
        self._f = f

    def __getitem__(self, *args, **kwargs):
        return self._f(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        raise TypeError('Missing applied type')

    def __repr__(self):
        return 'typed({})'.format(self._f)
