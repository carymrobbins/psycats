# -*- coding: utf-8 -*-
import inspect
from abc import ABCMeta
from collections import OrderedDict
from functools import wraps
from operator import attrgetter

import six

# noinspection PyProtectedMember
from psycats.meta.unsafe.registry import _TYPE_CLASS_REGISTRY


# noinspection PyPep8Naming
class classproperty(object):
    def __init__(self, f):
        self.f = f

    def __get__(self, obj, owner):
        return self.f(owner)


def extends(*typeclasses):

    def add_method(cls, tc, member_name):
        cached_instance_field_name = '_' + tc.__name__.lower() + '_cached'

        def method(self, *args, **kwargs):
            inst = getattr(cls, cached_instance_field_name, None)
            if inst is None:
                inst = tc[self._type_args]
                setattr(cls, cached_instance_field_name, inst)
            return getattr(inst, member_name)(*args, **kwargs)
        method.__name__ = member_name
        setattr(cls, member_name, method)

    def wrapper(cls):
        for tc in typeclasses:
            for member_name in dir(tc):
                if not member_name.startswith('_') and not hasattr(cls, member_name):
                    add_method(cls, tc, member_name)
        return cls
    return wrapper


class ComposableMeta(ABCMeta):
    def __and__(cls, o):
        class Meta(type(cls), type(o)):
            pass

        class Ops(six.with_metaclass(Meta, cls, o)):
            pass

        Ops.__name__ = '__'.join(c.__name__ for c in (cls, o))
        return Ops


class constructor(object):
    def __init__(self, f):
        # noinspection PyDeprecation
        sig = inspect.getargspec(f)
        if sig.varargs:
            raise TypeError('ADT does not support varargs...yet')
        if sig.keywords:
            raise TypeError('ADT does not support kwargs...yet')
        if sig.defaults:
            raise TypeError('ADT does not support defaults...yet')
        self.__name__ = f.__name__
        self._f = f
        self._sig = sig
        self._typ = None

    def _set_type(self, typ):
        if self._typ:
            raise TypeError('constructor type already set')
        self._typ = typ

    def __call__(self, *args):
        result_args = OrderedDict()
        if len(args) != len(self._sig.args):
            raise TypeError(
                'Constructor {} expects {} arguments, got {}'
                    .format(self._f.__name__, len(sig.args), len(args)))
        for i, a in enumerate(args):
            try:
                result_args[self._sig.args[i]] = a
            except IndexError:
                raise TypeError('Too many arguments for constructor ' + self.__name__)
        return self._typ(self, result_args)


class ADTMeta(ComposableMeta):
    def __new__(mcs, name, bases, attrs):
        ctors = []
        for k, v in attrs.items():
            if isinstance(v, constructor):
                ctors.append(v)
        cls = super(ADTMeta, mcs).__new__(mcs, name, bases, attrs)
        cls._constructors = tuple(ctors)
        for ctor in ctors:
            ctor._set_type(cls)
            setattr(cls, ctor.__name__, ctor)
        return cls


class ADT(six.with_metaclass(ADTMeta)):

    _constructors = ()

    def __init__(self, ctor, args):
        self._ctor = ctor
        self._args = args

    def __getattr__(self, attr):
        if not attr.startswith('_') and attr in self._args:
            return self._args[attr]
        raise AttributeError(
            "'{}.{}' object has no attribute '{}'"
            .format(type(self).__name__, self._ctor.__name__, attr))


def get_constructors(typ):
    if not issubclass(typ, ADT):
        raise TypeError
    for c in typ._constructors:
        yield c


def switch(x):
    if not isinstance(x, ADT):
        raise TypeError('switch only supported for ADT values')

    def wrapper(cls):
        f = getattr(cls, x._ctor.__name__, None)
        if not f:
            raise TypeError('Unhandled switch case: {}'.format(x))
        return six.get_unbound_function(f)(*x._args.values())
    return wrapper


class TypeClassMeta(type):

    def __new__(mcs, name, bases, attrs):
        attrs['__getitem__'] = _typeclass_get_item
        attrs['_type_cache'] = {}
        return super(TypeClassMeta, mcs).__new__(mcs, name, bases, attrs)

    def __getitem__(self, item):
        return _typeclass_get_item(self, item)


def _typeclass_get_item(self, args):
    if isinstance(args, type):
        args = (args,)
    for i, x in enumerate(args, 1):
        if not isinstance(x, type):
            raise TypeError('Unexpected non-type at argument {}: {}'.format(i, type(x)))

    res = self._type_cache.get(args)
    if res is not None:
        return res

    name = '{}[{}]'.format(
        self.__name__, ''.join(map(lambda t: t.__name__, args)))

    class AppliedTypeClassMeta(type(self)):
        # Gets initialized automatically by summon[self] in __getattribute__
        _instance = None

        def __repr__(self):
            return name

        def __getattribute__(self, attr):
            if not attr.startswith('_') and attr != 'mro':
                inst = summon[self]
                if inst and hasattr(inst, attr):
                    return getattr(inst, attr)
            return super(AppliedTypeClassMeta, self).__getattribute__(attr)

    class AppliedTypeClass(six.with_metaclass(AppliedTypeClassMeta, self)):
        _type_args = args

    AppliedTypeClass.__name__ = name
    self._type_cache[args] = AppliedTypeClass
    return AppliedTypeClass


class TypeClass(six.with_metaclass(TypeClassMeta)):
    _type_args = ()


class Summoner(object):
    def __getitem__(self, k):
        res = self.get(k)
        if res is None:
            raise TypeError('No instance found for {}'.format(k))
        return res

    # noinspection PyMethodMayBeStatic
    def get(self, k):
        if not isinstance(k, type):
            raise TypeError('Expected a type, got a {} value'.format(type(k)))
        res = getattr(k, '_instance', None)
        if res:
            k._instance = res
            return res
        res = _TYPE_CLASS_REGISTRY.get(k)
        return res


summon = Summoner()


def instance(typ):
    # Guess at typeclass.
    k = typ.mro()[1]
    if k in _TYPE_CLASS_REGISTRY:
        raise TypeError('Duplicate instance defined for {}'.format(k))
    inst = typ()
    _TYPE_CLASS_REGISTRY[k] = inst
    return inst
