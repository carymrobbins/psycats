# -*- coding: utf-8 -*-
from psycats.prelude import *


# noinspection PyPep8Naming
@typed
def div(F):
    @do(F)
    def div_impl(n, d):
        n = yield pure(n)
        d = yield pure(d)
        if d == 0:
            yield MonadZero[F].mzero()
        yield pure(n / d)
    return div_impl


@typed
def div2(F):
    @do(F)
    def div_impl(n, d):
        n = yield pure(n)
        d = yield pure(d)
        if d == 0:
            yield MonadZero[F].mzero()
        # You can use return!
        return n / d
    return div_impl


def main():
    print(div[list](1, 2))    # [0.5]
    print(div[Maybe](1, 2))   # Just(0.5)

    print(div[list](1, 0))    # []
    print(div[Maybe](1, 0))   # Nothing

    @block
    @do
    def result():
        yield Maybe(1)
        yield Nothing
        yield Maybe(2)

    print('block result: {}'.format(result))


if __name__ == '__main__':
    main()
