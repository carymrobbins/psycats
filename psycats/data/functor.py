# -*- coding: utf-8 -*-
from abc import abstractmethod

import six

from psycats.data.function import const
from psycats.meta.types import TypeClass, instance, ComposableMeta


class Functor(TypeClass):
    @abstractmethod
    def map(self, f, fa): pass

    def of(self, a, fb):
        return self.map(const(a), fb)


class FunctorOpsMeta(ComposableMeta):
    def __new__(mcs, name, bases, attrs):
        cls = super(FunctorOpsMeta, mcs).__new__(mcs, name, bases, attrs)
        methods = {'map': lambda self, f, fa: fa.map(f)}
        if 'of' in attrs:
            methods['of'] = lambda self, a, fb: fb.of(a)
        cls._functor = instance(
            type('Functor[{}]'.format(name), (Functor[cls],), methods))
        return cls


class FunctorOps(six.with_metaclass(FunctorOpsMeta)):

    @abstractmethod
    def map(self, f): pass

    def of(self, a): return self._functor.of(a, self)


def fmap(f, fa): return Functor[type(fa)].map(f, fa)


def of(a, fb): return Functor[type(fb)].of(a, fb)


@instance
class FunctorList(Functor[list]):
    def map(self, f, fa):
        res = []
        for x in fa:
            res.append(f(x))
        return res
