# -*- coding: utf-8 -*-
from abc import abstractmethod

from psycats.meta.types import TypeClass, summon, instance, extends
from psycats.syntax.typed import typed


class Semigroup(TypeClass):
    @abstractmethod
    def combine(self, x, y): pass


def combine(x, y):
    return Semigroup[type(x)].combine(x, y)


@instance
class SemigroupStr(Semigroup[str]):
    def combine(self, x, y): return x + y


@instance
class SemigroupList(Semigroup[list]):
    def combine(self, x, y): return x + y


@instance
class SemigroupSet(Semigroup[set]):
    def combine(self, x, y): return x.union(y)


@instance
class SemigroupDict(Semigroup[dict]):
    def combine(self, x, y):
        res = {}
        res.update(x)
        res.update(y)
        return res


@extends(Semigroup)
class Monoid(TypeClass):
    @abstractmethod
    def empty(self): pass


@typed
def empty(A): return Monoid[A].empty()


@instance
class MonoidStr(Monoid[str]):
    def empty(self): return ''


@instance
class MonoidList(Monoid[list]):
    def empty(self): return []


@instance
class MonoidSet(Monoid[set]):
    def empty(self): return set()


@instance
class MonoidDict(Monoid[dict]):
    def empty(self): return {}
