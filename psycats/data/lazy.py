# -*- coding: utf-8 -*-


def lazy(f):
    return LazyRef(f).value


class LazyRef(object):
    def __init__(self, f):
        self._f = f

    def value(self):
        if hasattr(self, '_value'):
            return self._value
        # noinspection PyAttributeOutsideInit
        self._value = self._f()
        return self._value
