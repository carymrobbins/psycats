# -*- coding: utf-8 -*-
from psycats.control.monaderror import MonadErrorOps
from psycats.data.eq import EqOpsADT
from psycats.data.show import ShowOpsADT
from psycats.meta.types import ADT, constructor, switch, get_constructors


class Either(ADT & ShowOpsADT & EqOpsADT & MonadErrorOps):

    @constructor
    def Left(_a): pass

    @constructor
    def Right(_b): pass

    @property
    def unsafe_get(self):
        @switch(self)
        class result:
            def Left(_): raise ValueError('Either.Left')
            def Right(a): return a
        return result

    @property
    def merge(self):
        @switch(self)
        class result:
            def Left(a): return a
            def Right(b): return b
        return result

    def fold(self, f, g):
        @switch(self)
        class result:
            def Left(a): return f(a)
            def Right(b): return g(b)
        return result

    def get_or(self, f):
        @switch(self)
        class result:
            def Left(a): return f(a)
            def Right(b): return b
        return result

    def map(self, f):
        @switch(self)
        class result:
            def Left(_): return self
            def Right(a): return Right(f(a))
        return result

    def leftmap(self, f):
        @switch(self)
        class result:
            def Left(a): return Left(f(a))
            def Right(_): return self
        return result

    def bimap(self, f, g):
        @switch(self)
        class result:
            def Left(a): return Left(f(a))
            def Right(b): return Right(g(b))
        return result

    @property
    def swap(self):
        @switch(self)
        class result:
            def Left(a): return Right(a)
            def Right(b): return Left(b)
        return result

    def is_left(self):
        return self._ctor is Left

    def is_right(self):
        return self._ctor is Right

    @classmethod
    def pure(cls, x): return Right(x)

    @classmethod
    def error(cls, e): return Left(e)

    def bind(self, f):
        @switch(self)
        class result:
            def Left(_): return self
            def Right(a): return f(a)
        return result


Left, Right = get_constructors(Either)
