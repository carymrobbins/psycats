# -*- coding: utf-8 -*-
from psycats.control.monad import MonadOps
from psycats.data.eq import EqOps, eq
from psycats.data.lazy import lazy
from psycats.data.show import ShowOps, show


class Eval(EqOps & ShowOps & MonadOps):
    def __init__(self, impl):
        self._impl = impl

    @classmethod
    def now(cls, x): return cls(lambda: x)

    @classmethod
    def later(cls, f): return cls(lazy(f))

    @classmethod
    def always(cls, f): return cls(f)

    @property
    def value(self): return self._impl()

    @classmethod
    def pure(cls, a): return Eval.now(a)

    # TODO: This is inefficient and probably wrong.
    def bind(self, f): return Eval.later(lambda: f(self.value).value)

    def eq(self, o):
        if not isinstance(o, Eval):
            raise TypeError
        return eq(self.value, o.value)

    def show(self):
        return 'Eval({})'.format(show(self.value))
