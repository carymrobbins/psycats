# -*- coding: utf-8 -*-
from psycats.control.monadzero import MonadZeroOps
from psycats.data.eq import Eq, EqOps
from psycats.data.show import Show, ShowOps


class Maybe(MonadZeroOps & EqOps & ShowOps):
    def __init__(self, value):
        self._value = value

    def __new__(cls, value):
        if value is None:
            return Nothing
        return super(Maybe, cls).__new__(cls)

    @classmethod
    def pure(cls, x): return cls(x)

    @classmethod
    def mzero(cls): return Nothing

    @classmethod
    def coalesce(cls, x):
        if isinstance(x, Maybe):
            return x
        return Maybe(x)

    @property
    def get_or_none(self): return self._value

    @property
    def lens(self):
        return MaybeLens(self._value)

    def fold(self, df, f):
        if self:
            return df()
        return f(self._value)

    def map(self, f):
        return self and self.pure(f(self._value))

    def filter(self, f=lambda x: x):
        if not self:
            return Nothing
        if not f(self._value):
            return Nothing
        return self

    def ap(self, ma):
        if not self or not ma:
            return Nothing
        # noinspection PyProtectedMember
        return Maybe(self._value(ma._value))

    def bind(self, f):
        if not self:
            return Nothing
        return f(self._value)

    def __nonzero__(self):
        return self._value is not None

    def __bool__(self):
        return self._value is not None

    def __iter__(self):
        if self:
            yield self._value

    def __eq__(self, other):
        if isinstance(other, Maybe):
            return self._value == other._value
        return self._value == other

    def __or__(self, other):
        if self:
            return self
        if isinstance(other, Maybe):
            return other
        return self.pure(other)

    def __and__(self, other):
        if not self:
            return self
        if isinstance(other, Maybe):
            return other
        return self.pure(other)

    def eq(self, o):
        if not isinstance(o, Maybe):
            raise TypeError
        if self is Nothing:
            return o is Nothing
        return Eq[type(self._value)].eq(self._value, o._value)

    def show(self):
        if self is Nothing:
            return 'Nothing'
        return 'Just({})'.format(
            Show[type(self._value)].show(self._value))


Nothing = Maybe(0)
Nothing._value = None


class MaybeLens(Maybe):

    def __getattr__(self, attr):
        return self & getattr(self._value, attr)

    def __getitem__(self, item):
        return self & self._value[item]

    def __call__(self, *args, **kwargs):
        return self & self._value(*args, **kwargs)

    @property
    def unlens(self):
        return Maybe(self._value)
