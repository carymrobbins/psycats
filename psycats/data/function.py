# -*- coding: utf-8 -*-


# id :: a -> a
def identity(a): return a


# const :: a -> b -> a
def const(a): return lambda *args, **kwargs: a


# ($) :: (a -> b) -> a -> b
def call(f): return lambda *args, **kwargs: f(*args, **kwargs)


# flip ($) :: a -> (a -> c) -> c
def applying(*args, **kwargs): return lambda f: f(*args, **kwargs)


# (.) :: (b -> c) -> (a -> b) -> a -> c
def compose(f): return lambda g: lambda *args, **kwargs: f(g(*args, **kwargs))
