# -*- coding: utf-8 -*-
from abc import abstractmethod

import six

from psycats.meta.types import TypeClass, instance, ComposableMeta, ADT


class Show(TypeClass):
    @abstractmethod
    def show(self, x): pass

    def magic_repr(self, x, fallback=object.__repr__):
        try:
            return self.show(x)
        except TypeError:
            return fallback(x)


def show(x):
    return Show[type(x)].show(x)


class ShowOpsMeta(ComposableMeta):
    def __new__(mcs, name, bases, attrs):
        cls = super(ShowOpsMeta, mcs).__new__(mcs, name, bases, attrs)
        methods = {'show': lambda self, x: x.show()}
        cls._show = instance(
            type('Show[{}]'.format(name), (Show[cls],), methods))
        return cls


class ShowOps(six.with_metaclass(ShowOpsMeta)):
    @abstractmethod
    def show(self): pass

    def __repr__(self):
        try:
            return self.show()
        except TypeError:
            return super(ShowOps, self).__repr__()


class ShowOpsADT(ShowOps):
    def show(self):
        return '{}({})'.format(
            self._ctor.__name__, ', '.join(map(repr, self._args.values())))


@instance
class ShowStr(Show[str]):
    def show(self, x): return repr(x)


@instance
class ShowInt(Show[int]):
    def show(self, x): return repr(x)


@instance
class ShowInt(Show[float]):
    def show(self, x): return repr(x)
