# -*- coding: utf-8 -*-
from abc import abstractmethod

import six

from psycats.meta.types import TypeClass, instance, ComposableMeta, ADT


class Eq(TypeClass):
    @abstractmethod
    def eq(self, x, y): pass

    def ne(self, x, y): return not self.eq(x, y)

    def magic_eq(self, x, y):
        try:
            return self.eq(x, y)
        except TypeError:
            return False

    def magic_ne(self, x, y):
        try:
            return self.ne(x, y)
        except TypeError:
            return True


def eq(x, y): return Eq[type(x)].eq(x, y)


def ne(x, y): return Eq[type(x)].ne(x, y)


class EqOpsMeta(ComposableMeta):
    def __new__(mcs, name, bases, attrs):
        cls = super(EqOpsMeta, mcs).__new__(mcs, name, bases, attrs)
        methods = {'eq': lambda self, x, y: x.eq(y) }
        if 'ne' in attrs:
            methods['ne'] = lambda self, x, y: x.ne(y)
        cls._eq = instance(
            type('Eq[{}]'.format(name), (Eq[cls],), methods))
        return cls


class EqOps(six.with_metaclass(EqOpsMeta)):
    @abstractmethod
    def eq(self, o): pass

    def ne(self, o): return not self.eq(o)

    def __eq__(self, o): return self._eq.magic_eq(self, o)

    def __ne__(self, o): return self._eq.magic_ne(self, o)


class EqOpsADT(EqOps):
    def eq(self, o):
        if not isinstance(o, type(self)):
            raise TypeError('Expected {}, got {}'.format(type(self), type(o)))
        return (self._ctor, self._args) == (o._ctor, o._args)


@instance
class EqInt(Eq[int]):
    def eq(self, x, y): return x == y

    def ne(self, x, y): return x != y


@instance
class EqFloat(Eq[float]):
    def eq(self, x, y): return x == y

    def ne(self, x, y): return x != y


@instance
class EqStr(Eq[str]):
    def eq(self, x, y): return x == y

    def ne(self, x, y): return x != y

