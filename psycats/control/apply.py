# -*- coding: utf-8 -*-
from abc import abstractmethod

import six

from psycats.data.function import identity, const
from psycats.data.functor import Functor, FunctorOps, FunctorOpsMeta
from psycats.meta.types import extends, TypeClass, instance


@extends(Functor)
class Apply(TypeClass):
    @abstractmethod
    def ap(self, ff, fa): pass

    def lift_a2(self, f, fa, fb):
        return self.ap(self.map(f, fa), fb)

    def product_r(self, fa, fb):
        return self.ap(self.of(identity, fa), fb)

    def product_l(self, fa, fb):
        return self.lift_a2(const, fa, fb)

    def product(self, fa, fb):
        return self.ap(self.map(lambda a: lambda b: (a, b), fa), fb)


class ApplyOpsMeta(FunctorOpsMeta):
    def __new__(mcs, name, bases, attrs):
        cls = super(ApplyOpsMeta, mcs).__new__(mcs, name, bases, attrs)
        methods = {'ap': lambda self, ff, fa: ff.ap(fa)}
        if 'lift_a2' in attrs:
            methods['lift_a2'] = lambda self, f, fa, fb: cls.lift_a2(f, fa, fb)
        if 'product_r' in attrs:
            methods['product_r'] = lambda self, fa, fb: fa.product_r(fb)
        if 'product_l' in attrs:
            methods['product_l'] = lambda self, fa, fb: fa.product_l(fb)
        if 'product' in attrs:
            methods['product'] = lambda self, fa, fb: fa.product(fb)
        cls._apply = instance(
            type('Apply[{}]'.format(name), (Apply[cls],), methods))
        return cls


class ApplyOps(six.with_metaclass(ApplyOpsMeta, FunctorOps)):
    @abstractmethod
    def ap(self, ff): pass

    def product_r(self, fb): return self._apply.product_r(self, fb)

    def product_l(self, fb): return self._apply.product_l(self, fb)

    def product(self, fb): return self._apply.product(self, fb)

    def __rshift__(self, fb): return self.product_r(fb)

    def __lshift__(self, fb): return self.product_l(fb)


def ap(ff, fa): return Apply[type(ff)].ap(ff, fa)


def lift_a2(f, fa, fb): return Apply[type(fa)].lift_a2(f, fa, fb)


def product_r(fa, fb): return Apply[type(fa)].product_r(fa, fb)


def product_l(fa, fb): return Apply[type(fa)].product_l(fa, fb)


@instance
class ApplyList(Apply[list]):
    def ap(self, ff, fa):
        res = []
        for f in ff:
            for x in fa:
                res.append(f(x))
        return res
