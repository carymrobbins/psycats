# -*- coding: utf-8 -*-
from abc import abstractmethod

import six

from psycats.control.monad import Monad, MonadOps, MonadOpsMeta
from psycats.meta.types import extends, TypeClass, instance
from psycats.syntax.typed import typed


@extends(Monad)
class MonadZero(TypeClass):
    @abstractmethod
    def mzero(self): pass


# noinspection PyPep8Naming
@typed
def mzero(F): return MonadZero[F].mzero()


class MonadZeroOpsMeta(MonadOpsMeta):
    def __new__(mcs, name, bases, attrs):
        cls = super(MonadZeroOpsMeta, mcs).__new__(mcs, name, bases, attrs)
        methods = {'mzero': lambda self: cls.mzero()}
        cls._monadzero = instance(
            type('MonadZero[{}]'.format(name), (MonadZero[cls],), methods))
        return cls


class MonadZeroOps(six.with_metaclass(MonadZeroOpsMeta, MonadOps)):
    @classmethod
    @abstractmethod
    def mzero(cls): pass


@instance
class MonadZeroList(MonadZero[list]):
    def mzero(self): return []
