# -*- coding: utf-8 -*-
import six

from psycats.control.applicative import Applicative, ApplicativeOps, ApplicativeOpsMeta
from psycats.control.bind import Bind, BindOps, BindOpsMeta
from psycats.meta.types import extends, TypeClass, instance


@extends(Applicative, Bind)
class Monad(TypeClass):
    pass


class MonadOpsMeta(ApplicativeOpsMeta, BindOpsMeta):
    def __new__(mcs, name, bases, attrs):
        cls = super(MonadOpsMeta, mcs).__new__(mcs, name, bases, attrs)
        cls._monad = instance(
            type('Monad[{}]'.format(name), (Monad[cls],), {}))
        return cls


class MonadOps(six.with_metaclass(MonadOpsMeta, ApplicativeOps, BindOps)):
    def map(self, f): return self.bind(lambda a: self.pure(f(a)))


@instance
class MonadList(Monad[list]):
    pass
