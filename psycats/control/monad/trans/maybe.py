# -*- coding: utf-8 -*-
from psycats.control.bind import bind
from psycats.control.monad import MonadOps
from psycats.data.eq import EqOps, eq
from psycats.data.functor import Functor, fmap
from psycats.data.maybe import Maybe, Nothing
from psycats.data.show import ShowOps, show
from psycats.meta.types import instance


class MaybeT(EqOps & ShowOps & MonadOps):
    def __init__(self, value):
        """f (Maybe a) -> MaybeT f a"""
        self._value = value

    @property
    def value(self): return self._value

    def map(self, f):
        return MaybeT(
            Functor[type(self.value)].map(
                lambda x: Maybe.coalesce(x).map(f),
                self.value))

    def eq(self, o):
        if not isinstance(o, MaybeT):
            raise TypeError
        return eq(self.value, o.value)

    def show(self):
        return 'MaybeT({})'.format(show(self.value))

    @classmethod
    def pure(cls):
        # Need to figure out how to apply a type here
        raise NotImplementedError

    def fold(self, df, f):
        return fmap(lambda x: x.fold(df, f), self.value)

    def bind_f(self, f):
        return MaybeT(bind(self.value, lambda x: x.fold(lambda: Nothing, f)))

    def bind(self, f):
        self.bind_f(lambda a: f(a).value)
