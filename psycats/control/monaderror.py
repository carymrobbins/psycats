# -*- coding: utf-8 -*-
import six

from psycats.control.applicativeerror import (
    ApplicativeError, ApplicativeErrorOps,
    ApplicativeErrorOpsMeta,
)
from psycats.control.monad import Monad, MonadOps, MonadOpsMeta
from psycats.meta.types import extends, TypeClass, instance


@extends(ApplicativeError, Monad)
class MonadError(TypeClass):
    pass


class MonadErrorOpsMeta(ApplicativeErrorOpsMeta, MonadOpsMeta):
    def __new__(mcs, name, bases, attrs):
        cls = super(MonadErrorOpsMeta, mcs).__new__(mcs, name, bases, attrs)
        cls._monaderror = instance(
            type('MonadError[{}]'.format(name), (MonadError[cls],), {}))
        return cls


class MonadErrorOps(six.with_metaclass(
        MonadErrorOpsMeta, ApplicativeErrorOps, MonadOps)):

    def ensure(self, ef, p):
        return self.bind(lambda a: self.pure(a) if p(a) else self.error(ef()))

    def ensure_or(self, ef, p):
        return self.bind(lambda a: self.pure(a) if p(a) else self.error(ef(a)))
