# -*- coding: utf-8 -*-
from abc import abstractmethod

import six

from psycats.control.applicative import ApplicativeOps, ApplicativeOpsMeta, Applicative
from psycats.meta.types import TypeClass, instance, extends


@extends(Applicative)
class ApplicativeError(TypeClass):
    @abstractmethod
    def error(self, e): pass


class ApplicativeErrorOpsMeta(ApplicativeOpsMeta):
    def __new__(mcs, name, bases, attrs):
        cls = super(ApplicativeErrorOpsMeta, mcs).__new__(mcs, name, bases, attrs)
        methods = {'error': lambda self, e: cls.error(e) }
        cls._applicativeerror = instance(
            type('ApplicativeError[{}]'.format(name), (ApplicativeError[cls],), methods))
        return cls


class ApplicativeErrorOps(six.with_metaclass(ApplicativeErrorOpsMeta, ApplicativeOps)):
    @classmethod
    @abstractmethod
    def error(cls, e): pass
