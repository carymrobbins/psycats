# -*- coding: utf-8 -*-
from abc import abstractmethod

import six

from psycats.control.apply import Apply, ApplyOps, ApplyOpsMeta
from psycats.meta.types import extends, TypeClass, instance


@extends(Apply)
class Applicative(TypeClass):
    @abstractmethod
    def pure(self, a): pass


class ApplicativeOpsMeta(ApplyOpsMeta):
    def __new__(mcs, name, bases, attrs):
        cls = super(ApplicativeOpsMeta, mcs).__new__(mcs, name, bases, attrs)
        methods = {'pure': lambda self, a: cls.pure(a)}
        cls._applicative = instance(
            type('Applicative[{}]'.format(name), (Applicative[cls],), methods))
        return cls


class ApplicativeOps(six.with_metaclass(ApplicativeOpsMeta, ApplyOps)):
    @classmethod
    @abstractmethod
    def pure(cls, a): pass

    def map(self, f):
        return self.pure(f).ap(self)


@instance
class ApplicativeList(Applicative[list]):
    def pure(self, a): return [a]


class PureMeta(type):
    def __getitem__(self, typ):
        return Applicative[typ].pure


# noinspection PyPep8Naming
class pure(six.with_metaclass(PureMeta)):
    def __init__(self, value):
        self.value = value

    def __getitem__(self, typ):
        if not isinstance(typ, type):
            raise TypeError('Expected type got {}: {}'.format(type(type), typ))
        return pure[typ](self.value)
