# -*- coding: utf-8 -*-
from abc import abstractmethod

import six

from psycats.control.apply import Apply, ApplyOps, ApplyOpsMeta
from psycats.meta.types import extends, TypeClass, instance


@extends(Apply)
class Bind(TypeClass):
    @abstractmethod
    def bind(self, fa, f): pass


class BindOpsMeta(ApplyOpsMeta):
    def __new__(mcs, name, bases, attrs):
        cls = super(BindOpsMeta, mcs).__new__(mcs, name, bases, attrs)
        methods = {'bind': lambda self, fa, f: fa.bind(f)}
        cls._bind = instance(
            type('Bind[{}]'.format(name), (Bind[cls],), methods))
        return cls


class BindOps(six.with_metaclass(BindOpsMeta, ApplyOps)):
    @abstractmethod
    def bind(self, f): pass

    def ap(self, fa): return self.bind(fa.map)


def bind(fa, f): return Bind[type(fa)].bind(fa, f)


@instance
class BindList(Bind[list]):
    def bind(self, fa, f):
        res = []
        for x in fa:
            res.extend(f(x))
        return res
