# -*- coding: utf-8 -*-
from psycats.control.monad.trans.maybe import MaybeT
from psycats.data.eval import Eval
from psycats.data.function import identity
from psycats.data.functor import fmap
from psycats.data.maybe import Maybe


def test_functor_identity_law():
    x = MaybeT(Eval.now(Maybe(3)))
    assert fmap(identity, x) == identity(x)
