# -*- coding: utf-8 -*-
from unittest import TestCase

from psycats.control.applicative import pure
from psycats.control.monadzero import mzero
from psycats.data.function import identity, applying, compose
from psycats.data.maybe import Maybe, Nothing


class TestMaybe(TestCase):

    # pure id <*> v = v
    def test_applicative_identity_law(self):
        v = 1
        assert Maybe(identity).ap(Maybe(v)) == Maybe(v)

    # pure f <*> pure x = pure (f x)
    def test_applicative_homomorphism_law(self):
        def f(x): return x * 2
        v = 3
        assert Maybe(f).ap(Maybe(v)) == Maybe(f(v))

    # u <*> pure y = pure ($ y) <*> u
    def test_applicative_interchange_law(self):
        u = Maybe(lambda x: x * 2)
        y = 3
        assert u.ap(Maybe(y)) == Maybe(applying(y)).ap(u)

    # pure (.) <*> u <*> v <*> w = u <*> (v <*> w)
    def test_applicative_composition_law(self):
        u = Maybe(lambda x: x - 2)
        v = Maybe(lambda x: x * 3)
        w = Maybe(4)
        assert Maybe(compose).ap(u).ap(v).ap(w) == u.ap(v.ap(w))

    def test_apply_ops(self):
        assert Maybe(1) << Maybe(2) == Maybe(1)
        assert Maybe(1) >> Maybe(2) == Maybe(2)
        assert Maybe(1).product(Maybe(2)) == Maybe((1, 2))

    def test_pure(self):
        assert pure[Maybe](1) == Maybe(1)

    def test_mzero(self):
        assert mzero[Maybe] == Nothing
