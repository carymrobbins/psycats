# -*- coding: utf-8 -*-
from unittest import TestCase

from psycats.data.eval import Eval
from psycats.syntax.block import block
from psycats.syntax.do import do


class TestEval(TestCase):

    def test_eval_eq(self):
        assert Eval.now(1) == Eval.now(1)

    def test_eval_monad(self):
        @block
        @do(Eval)
        def result():
            x = yield Eval.now(1)
            y = yield Eval.later(lambda: 2)
            yield Eval.always(lambda: x + y)

        assert result.value == 3
