# -*- coding: utf-8 -*-
from unittest import TestCase

from psycats.control.monad import Monad
from psycats.data.maybe import Maybe


class TestExtends(TestCase):
    def test_extends_in_base(self):
        # Monad extends Functor
        assert Monad[Maybe].map(lambda x: x*2, Maybe(2)) == Maybe(4)
        # Monad extends Applicative
        assert Monad[Maybe].pure(1) == Maybe(1)
        # Monad extends Bind
        assert Monad[Maybe].bind(Maybe(1), lambda x: Maybe(x+2)) == Maybe(3)
