# -*- coding: utf-8 -*-
import pytest

from psycats.control.monaderror import MonadError
from psycats.data.either import Either, Left, Right


def test_eq():
    assert Right(1) == Right(1)


def test_either_repr():
    assert repr(Left(1)) == 'Left(1)'
    assert repr(Right('foo')) == "Right('foo')"


# noinspection PyStatementEffect
def test_either_unsafe_get():
    assert Right(1).unsafe_get == 1
    with pytest.raises(ValueError):
        Left('foo').unsafe_get


def test_either_merge():
    assert Left('foo').merge == 'foo'
    assert Right(1).merge == 1


def test_either_get_or():
    assert Left('foo').get_or(lambda x: x + '!') == 'foo!'
    assert Right(1).get_or(lambda x: x + '!') == 1


def test_either_leftmap():
    assert Left('foo').leftmap(lambda x: x + '!') == Left('foo!')
    assert Right(1).leftmap(lambda x: x + '!') == Right(1)


def test_either_bimap():
    assert Left('foo').bimap(lambda x: x + '!', lambda x: x * 2) == Left('foo!')
    assert Right(1).bimap(lambda x: x + '!', lambda x: x * 2) == Right(2)


def test_either_swap():
    assert Left('foo').swap == Right('foo')
    assert Right(1).swap == Left(1)


def test_either_error():
    assert MonadError[Either].error('foo') == Left('foo')


def test_either_fold():
    assert Left(1).fold(lambda x: x + 1, lambda x: x + 2) == 2
    assert Right(2).fold(lambda x: x + 1, lambda x: x + 2) == 4
